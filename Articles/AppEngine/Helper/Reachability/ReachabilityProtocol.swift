//
//  ReachabilityProtocol.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import Foundation

protocol ReachabilityProtocol {
    
    /*
     Use this function to check internet connection
     **/
    func isInternetConnected() -> Bool
}
