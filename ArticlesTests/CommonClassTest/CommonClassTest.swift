//
//  CommonClassTest.swift
//  ArticlesTests
//
//  Created by sushantkumar01 on 04/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import XCTest
@testable import Articles

class CommonClassTest: XCTestCase {

    var commonClass: CommonClass!

    override func setUp() {
        super.setUp()
        commonClass = CommonClass()
    }

    override func tearDown() {
        commonClass = nil
        super.tearDown()
    }

    func testShowLoader() {
        commonClass.isProgressViewAdded = false
        commonClass.showLoader()
        XCTAssertTrue(commonClass.isProgressViewAdded == true)
    }
    
    func testHideLoader() {
        commonClass.isProgressViewAdded = true
        commonClass.hideLoader()
        XCTAssertTrue(commonClass.isProgressViewAdded == false)
    }    
}
