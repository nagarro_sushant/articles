//
//  AppConstants.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import Foundation
import UIKit

// MARK: - App Constants
struct AppConstants {
    static let sharedAppDelegate = UIApplication.shared.delegate as! AppDelegate
    static let kEmptyString = ""
}

// MARK: - App Default Placeholder Image Constants
struct AppPlaceholderImageConstants {
    static let article: UIImage = #imageLiteral(resourceName: "article_placeholder")
}

// MARK: - API Query Constant
struct APIQueryConstant {
    static let kAPIKey: String   = "api-key"
    static let APIKey = "fj7mjzSHZCJeHTZhJqqBnSENrqa1BVta"
}

// MARK: - Alert Messages
struct AlertMessage {
    static let noData = "No result found"
    static let noInternet = "Please check your internet connection."
}

// MARK: - View Constraints Constants
struct ViewConstraintConstants {
    static let paddingConstant: CGFloat        = 10
    static let zeroPaddingConstant: CGFloat    = 0
    static let imageHeight: CGFloat            = 90
    static let cellImageHeight: CGFloat        = 60
}

// MARK: - Screen Tite
struct ScreenTitles {
    static let articles = "Articles"
    static let articleDetail = "Article Detail"
}
