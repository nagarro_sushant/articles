//
//  APIManagerTest.swift
//  ArticlesTests
//
//  Created by sushantkumar01 on 04/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import XCTest
@testable import Articles

class APIManagerTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetArticlesFromServer() {
        StubObject.request(withPathRegex: "api.nytimes.com", withResponseFile: "Article.json")
        let expectedResult = expectation(description: "got result")
        APIManager().getArticlesFromServer { (result) in
            switch result {
            case .success(let items):
                XCTAssertEqual(2, items.results.count)
                expectedResult.fulfill()
            case .failure(let error):
                XCTAssertNotNil(error, "Failed to get response from list webservice")
            }
        }
        waitForExpectations(timeout: 60) { error in
            if let error = error {
                XCTAssertNotNil(error, "Failed to get response from list webservice")
            }
        }
    }
    
    func testGetDeliveriesFromServerWithErr() {
        StubObject.request(withPathRegex: "api.nytimes.com", withResponseFile: "InvalidData.json")
        let expectedResult = expectation(description: "got invalid")
        APIManager().getArticlesFromServer { (result) in
            switch result {
            case .success( let item):
                XCTAssertNil(item, "error: item should be nil")
            case .failure(let error):
                XCTAssertNotNil(error, "error: Expectation fulfilled with error")
                expectedResult.fulfill()
            }
        }
        waitForExpectations(timeout: 60) { error in
            if let error = error {
                XCTAssertNotNil(error, "Failed to get response from server")
            }
        }
    }
}
