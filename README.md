# ARTICLES

•    Shows list of popular articles. Detail page shows articles detail.

# INSTALLATION

•    Open Podfile from project directory.

•    Open terminal and cd to the directory containing the Podfile.

•    Run the "pod install" command. (Incase of error: [!] CocoaPods could not find compatible versions".  Please use command: pod install --repo-update)

•    Open xcworkspace 


# REQUIREMENT

•    Xcode : 10.2

•    Supported OS version: iOS (10.x,11.x, 12.x)


# LANGUAGE

•    Swift 5.0


# VERSION

•    1.0


# DESIGN PATTERN

![MVVM_pattern](https://user-images.githubusercontent.com/37066441/73722889-9b31af80-474d-11ea-8b97-02f5eeb1a0c5.jpg)

•    MVVM
The Model View ViewModel (MVVM) is an architectural pattern. Also protocols are used to make the code loosely coupled, which makes code testable without subclassing the classes for mocking.

•    Model: 
A Model is responsible for exposing data in a way that is easily accessible. It manages data received from server. API manager used to create model to show the data on table view. 

•    View: 
View controllers come under this layer. View controller is responsible for laying out user interface and interact with users. View model object are used for calling view model methods, and completion handlers are used to receive the data and show them on table view.

•    ViewModel: 
All business logics are handled in view model. View model is responsible to update model, based on events received from view and pass data to view to update UI elements for user action.


# LIBRARIES USED

•    Alamofire

•    SVProgressHUD

•    OHHTTPStubs/Swift

•    SDWebImage

•    Toast-Swift


# LINTING

•    Integration of SwiftLint into an Xcode scheme to keep a codebase consistent and maintainable .

•    Install the swiftLint via brew and need to add a new "Run Script Phase" with:

if which swiftlint >/dev/null; then

swiftlint

else

echo "warning: SwiftLint not installed, download from https://github.com/realm/SwiftLint"

fi

•    .swiftlint.yml file is used for basic set of rules . It is placed inside the project folder.


# ASSUMPTIONS

•    App is designed for iPhones only.

•    App is tested on iPhone X, iPhone SE, iPhone 6s series, iPhone 7 series.


# FEATURES


•    REFRESH

-    Pull to refresh is implemented to refresh article list. 

-    In case of pull to refresh, refreshed data from server will be updated on table view.

•    ITEM DETAILS

-    User can get details of articles listed by tapping on the item. It will navigate to new screen. 

-    Detail screen shows the article detail.

•    CODING TECHNIQUE

-    Protocol oriented programming. 

-    Improves testability. 


# UNIT TESTING

•    Unit testing is done by using XCTest.

•    To run tests click Product->Test or (cmd+U)


# IMPROVEMENTS

•    UI could be done more interactive and user friendly.

•    UI Testing is not implemented.

# LICENSE

MIT License
Copyright (c) 2019
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

