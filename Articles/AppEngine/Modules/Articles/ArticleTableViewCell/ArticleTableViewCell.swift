//
//  ArticleTableViewCell.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import UIKit
import SDWebImage

class ArticleTableViewCell: UITableViewCell {
    
    // MARK: - UI Elements
    var articleImageView = UIImageView()
    var articleTitleLabel = UILabel()
    var articleDescriptionLabel = UILabel()
    var articleDateLabel = UILabel()

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super .init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupUI()
        self.layoutUIElements()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup UI
    private func setupUI() {
        articleImageView.clipsToBounds = true
        articleImageView.contentMode = UIView.ContentMode.scaleAspectFill
        
        articleTitleLabel.numberOfLines = 2
        articleTitleLabel.textAlignment = .left
        articleTitleLabel.textColor = UIColor.black
        
        articleDescriptionLabel.numberOfLines = 2
        articleDescriptionLabel.textAlignment = .left
        articleDescriptionLabel.textColor = UIColor.darkGray
        
        articleDateLabel.numberOfLines = 1
        articleDateLabel.textAlignment = .right
        articleDateLabel.textColor = UIColor.lightGray
    }
    
    // MARK: - Plot Data On Cell
    func plotDataOnCell(withCellArticle article: Article) {
        self.articleImageView.image = AppPlaceholderImageConstants.article

        guard let title = article.title else {
            self.articleTitleLabel.text = AppConstants.kEmptyString
            return
        }
        self.articleTitleLabel.text = title

        guard let desc = article.abstract else {
            self.articleDescriptionLabel.text = AppConstants.kEmptyString
            return
        }
        self.articleDescriptionLabel.text = desc
        
        guard let date = article.published_date else {
            self.articleDateLabel.text = AppConstants.kEmptyString
            return
        }
        self.articleDateLabel.text = date
    }
    
    // MARK: - Add Constraints To UI Elements
    func layoutUIElements() {
        self.selectionStyle = .none
        addSubview(articleImageView)
        addSubview(articleTitleLabel)
        addSubview(articleDescriptionLabel)
        addSubview(articleDateLabel)
        
        let cornerAnchorForImageView = ConrnerAnchor(top: (topAnchor, ViewConstraintConstants.paddingConstant),
                                                     bottom: (nil, ViewConstraintConstants.zeroPaddingConstant),
                                                     left: (leftAnchor, ViewConstraintConstants.paddingConstant),
                                                     right: (nil, ViewConstraintConstants.zeroPaddingConstant)
        )
        articleImageView.addConstraints(cornerConstraints: cornerAnchorForImageView,
                                        centerY: nil,
                                        centerX: nil,
                                        height: ViewConstraintConstants.cellImageHeight,
                                        width: ViewConstraintConstants.cellImageHeight
        )
        
        let cornerAnchorForTitleLabel = ConrnerAnchor(top: (topAnchor, ViewConstraintConstants.paddingConstant),
                                                      bottom: (nil, ViewConstraintConstants.zeroPaddingConstant),
                                                      left: (articleImageView.rightAnchor, ViewConstraintConstants.paddingConstant),
                                                      right: (rightAnchor, ViewConstraintConstants.paddingConstant)
        )
        articleTitleLabel.addConstraints(cornerConstraints: cornerAnchorForTitleLabel,
                                         centerY: nil,
                                         centerX: nil,
                                         height: ViewConstraintConstants.zeroPaddingConstant,
                                         width: ViewConstraintConstants.zeroPaddingConstant
        )
        
        let cornerConstraintForArticleDescriptionLabel = ConrnerAnchor(top: (articleTitleLabel.bottomAnchor, ViewConstraintConstants.paddingConstant),
                                                                       bottom: (nil, ViewConstraintConstants.zeroPaddingConstant),
                                                                       left: (articleImageView.rightAnchor, ViewConstraintConstants.paddingConstant),
                                                                       right: (rightAnchor, ViewConstraintConstants.paddingConstant))
        articleDescriptionLabel.addConstraints(cornerConstraints: cornerConstraintForArticleDescriptionLabel,
                                               centerY: nil,
                                               centerX: nil,
                                               height: ViewConstraintConstants.zeroPaddingConstant,
                                               width: ViewConstraintConstants.zeroPaddingConstant
        )
        
        let cornerConstraintForArticleDateLabel = ConrnerAnchor(top: (articleDescriptionLabel.bottomAnchor, ViewConstraintConstants.paddingConstant),
                                                                bottom: (bottomAnchor, ViewConstraintConstants.paddingConstant),
                                                                left: (articleImageView.rightAnchor, ViewConstraintConstants.paddingConstant),
                                                                right: (rightAnchor, ViewConstraintConstants.paddingConstant)
        )
        articleDateLabel.addConstraints(cornerConstraints: cornerConstraintForArticleDateLabel,
                                        centerY: nil,
                                        centerX: nil,
                                        height: ViewConstraintConstants.zeroPaddingConstant,
                                        width: ViewConstraintConstants.zeroPaddingConstant
        )
    }
}
