//
//  APIManager.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import Foundation
import Alamofire

class APIManager {
    
    // MARK: - Use this method to make api calls
    private class func apiService<T: Decodable>(url: String,
                                                parameter: [String: Any],
                                                completionHandler: @escaping ((Result<T, Error>) -> Void)) {
        AF.request(url, method: .get, parameters: parameter).responseDecodable(decoder: JSONDecoder()) { (response: DataResponse<T>)  in
            completionHandler(response.result)
        }
    }
}

// MARK: - Extension APIManagerProtocol
extension APIManager: APIManagerProtocol {
    
    // MARK: - Get Articles From Server
    func getArticlesFromServer(completionBlock: @escaping GetArticlesFromServerCompletion) {
        let url = APIConstants.baseURL + APIConstants.APIName.articles.description
        let param = [APIQueryConstant.kAPIKey: APIQueryConstant.APIKey]
        APIManager.apiService(url: url, parameter: param, completionHandler: completionBlock)
    }
}
