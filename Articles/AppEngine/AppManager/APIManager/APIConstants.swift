//
//  APIManager.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import Foundation

typealias GetArticlesFromServerCompletion = ((Result<AllArticle, Error>) -> Void)

struct APIConstants {
    // MARK: - Base URL
    static let baseURL: String = "http://api.nytimes.com/"
    
    public enum APIName: String {
        
        case articles
        
        var description: String {
            switch self {
            case .articles: return "svc/mostpopular/v2/mostviewed/all-sections/7.json"
            }
        }
    }
}
