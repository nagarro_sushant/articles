//
//  ArticleDetailViewControllerTest.swift
//  ArticlesTests
//
//  Created by sushantkumar01 on 04/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import XCTest
@testable import Articles

class ArticleDetailViewControllerTest: XCTestCase {

    var articleDetailVC: ArticleDetailViewController!
    
    override func setUp() {
        super.setUp()
        articleDetailVC = ArticleDetailViewController.init(ArticleDetailViewModel.init(withItem: getDummyItem()))
    }

    override func tearDown() {
        articleDetailVC = nil
        super.tearDown()
    }

    func testSetupNavigationBar() {
        articleDetailVC.setupNavigationBar()
        XCTAssertNotNil(articleDetailVC.title)
    }

    func testUIElementsShouldNotNil() {
        articleDetailVC.setupUIElements()
        XCTAssertNotNil(articleDetailVC.articleImageView)
        XCTAssertNotNil(articleDetailVC.articleTitleLabel)
        XCTAssertNotNil(articleDetailVC.articleDescriptionLabel)
        XCTAssertNotNil(articleDetailVC.articleDateLabel)
    }

    func testPlotDataWithNilTitle() {
        articleDetailVC.articleDetailVM.item.title = nil
        articleDetailVC.plotData()
        XCTAssertTrue(articleDetailVC.articleTitleLabel.text == AppConstants.kEmptyString)
    }
    
    func testPlotDataWithNilAbstract() {
        articleDetailVC.articleDetailVM.item.abstract = nil
        articleDetailVC.plotData()
        XCTAssertTrue(articleDetailVC.articleDescriptionLabel.text == AppConstants.kEmptyString)
    }
    
    func testPlotDataWithNilTime() {
        articleDetailVC.articleDetailVM.item.published_date = nil
        articleDetailVC.plotData()
        XCTAssertTrue(articleDetailVC.articleDateLabel.text == AppConstants.kEmptyString)
    }
    
    func testPlotDataWithNotNilItem() {
        articleDetailVC.plotData()
        XCTAssertTrue(articleDetailVC.articleDescriptionLabel.text != AppConstants.kEmptyString)
    }

}

// MARK: - Extension ArticleDetailViewControllerTest
extension ArticleDetailViewControllerTest {
    
    // MARK: - Get Dummy Article For Test
    func getDummyItem() -> Article {
        return Article.init(id: 0011, url: "", title: "Article dummy title", abstract: "Article dummy abstract.", published_date: "2019-08-10")
    }
}
