//
//  ArticlesViewControllerTest.swift
//  ArticlesTests
//
//  Created by sushantkumar01 on 04/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import XCTest
@testable import Articles

class ArticlesViewControllerTest: XCTestCase {

    var articlesVC: ArticleViewController!
    
    override func setUp() {
        super.setUp()
        let navigationController = AppConstants.sharedAppDelegate.window?.rootViewController as! UINavigationController
        articlesVC = navigationController.viewControllers[0] as? ArticleViewController
    }

    override func tearDown() {
        articlesVC = nil
        super.tearDown()
    }

    func testSetupNavigationBar() {
        articlesVC.setupNavigationBar()
        XCTAssertNotNil(articlesVC.title)
    }

    func testUIElementsShouldNotNil() {
        XCTAssertNotNil(articlesVC.articlesTableView)
        XCTAssertNotNil(articlesVC.navigationController)
    }

    func testSetupTableView() {
        articlesVC.setupTableView()
        XCTAssertNotNil(articlesVC.articlesTableView.refreshControl)
        XCTAssertNotNil(articlesVC.articlesTableView.dequeueReusableCell(withIdentifier: "ArticleCell"))
    }
    
    func testSetupCompletionHandlers() {
        articlesVC.setupCompletionHandlers()
        XCTAssertNotNil(articlesVC.viewModel.handleCompletionWithSuccess)
        XCTAssertNotNil(articlesVC.viewModel.handleCompletionWithNoData)
        XCTAssertNotNil(articlesVC.viewModel.handleCompletionWithError)
        XCTAssertNotNil(articlesVC.viewModel.handleInternetError)
    }
}
