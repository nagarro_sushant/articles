//
//  ArticleViewModelTest.swift
//  ArticlesTests
//
//  Created by sushantkumar01 on 04/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import XCTest
@testable import Articles

class ArticleViewModelTest: XCTestCase {

    var articleViewModel: ArticleViewModel!
    var mockapimanager: MockAPIManager!
    var mockReachability: MockReachability!

    override func setUp() {
        super.setUp()
        articleViewModel = ArticleViewModel()
        
        mockapimanager = MockAPIManager()
        articleViewModel?.apiManager = mockapimanager
        
        mockReachability = MockReachability()
        articleViewModel?.reachability = mockReachability
    }

    override func tearDown() {
        articleViewModel = nil
        mockapimanager = nil
        mockReachability = nil
        super.tearDown()
    }
    
    func testArticleDetailViewModel() {
        articleViewModel.articles = [getDummyItem()]
        XCTAssertNotNil(articleViewModel.getArticleDetailViewModel(fromIndex: 0))
    }
    
    func testArticleDetailNilViewModel() {
        articleViewModel.articles = [getDummyItem()]
        XCTAssertNil(articleViewModel.getArticleDetailViewModel(fromIndex: 1))
    }
    
    func testLoadData() {
        articleViewModel.isRequestInProgress = false
        articleViewModel.loadData()
        XCTAssertEqual(1, articleViewModel.articles.count)
    }
    
    func testResfreshData() {
        let article = getDummyItem()
        articleViewModel.articles = [article, article]
        articleViewModel.refreshData()
        XCTAssertEqual(1, articleViewModel.articles.count)
    }
}

// MARK: - Extension ArticleViewModelTest
extension ArticleViewModelTest {
    
    // MARK: - Get Dummy Item For Test
    func getDummyItem() -> Article {
        return Article.init(id: 1011, url: "", title: "Article dummy title", abstract: "Article dummy abstract.", published_date: "2019-08-10")
    }
}

// MARK: - MOCK API MANAGER
class MockAPIManager: APIManagerProtocol {
    
    var errorInResp = false
    
    func getArticlesFromServer(completionBlock: @escaping GetArticlesFromServerCompletion) {
        if errorInResp {
            let error = NSError(domain: "Error", code: 400, userInfo: nil)
            completionBlock(.failure(error))
            return
        } else {
            let allArticles = getAllArticles()
            completionBlock(.success(allArticles))
        }
    }
    
    func getAllArticles() -> AllArticle {
        let article = Article.init(id: 1011, url: "", title: "Article dummy title", abstract: "Article dummy abstract.", published_date: "2019-08-10")
        return AllArticle.init(num_results: 1, results: [article])
    }
}

// MARK: - MOCK CONECTIVITY
class MockReachability: ReachabilityProtocol {
    
    func isInternetConnected() -> Bool {
        return true
    }
}
