//
//  Article.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import Foundation

struct Article: Codable {
    
    var id: Int?
    var url: String?
    var title: String?
    var abstract: String?
    var published_date: String?
    
    init(id: Int?, url: String?, title: String?, abstract: String?, published_date: String?) {
        self.id = id
        self.url = url
        self.title = title
        self.abstract = abstract
        self.published_date = published_date
    }
}

struct AllArticle: Codable {
    
    var num_results: Int?
    var results: [Article]
    
    init(num_results: Int?, results: [Article]) {
        self.num_results = num_results
        self.results = results
    }
}
