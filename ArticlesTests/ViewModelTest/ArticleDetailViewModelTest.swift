//
//  ArticleDetailViewModelTest.swift
//  ArticlesTests
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import XCTest
@testable import Articles

class ArticleDetailViewModelTest: XCTestCase {

    var itemDetailVM: ArticleDetailViewModel!

    override func setUp() {
        super.setUp()
        itemDetailVM = ArticleDetailViewModel.init(withItem: getDummyItem())
    }

    override func tearDown() {
        itemDetailVM = nil
        super.tearDown()
    }
    
    func testItemShouldNotNil() {
        XCTAssertNotNil(itemDetailVM.item)
    }
}

// MARK: - Extension ArticleDetailViewModelTest
extension ArticleDetailViewModelTest {
    
    // MARK: - Get Dummy Article For Test
    func getDummyItem() -> Article {
        return Article.init(id: 0011, url: "", title: "Article dummy title", abstract: "Article dummy abstract.", published_date: "2019-08-10")
    }
}
