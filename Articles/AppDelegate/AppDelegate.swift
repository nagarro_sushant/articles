//
//  AppDelegate.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setInitialViewController()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
}

// MARK: - Extension Appdelegate
extension AppDelegate {
    
    // MARK: - Set Initial View Controller
    func setInitialViewController() {
        let rootVC = ArticleViewController()
        let navigationBarVC = UINavigationController(rootViewController: rootVC)
        AppConstants.sharedAppDelegate.window?.makeKeyAndVisible()
        AppConstants.sharedAppDelegate.window?.rootViewController = navigationBarVC
    }
}
