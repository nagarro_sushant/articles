//
//  ArticleDetailViewModel.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import Foundation

class ArticleDetailViewModel {
    
    var item: Article!
    
    init(withItem model: Article) {
        self.item = model
    }
}
