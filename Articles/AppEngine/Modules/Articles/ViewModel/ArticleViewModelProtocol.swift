//
//  ArticleViewModelProtocol.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import Foundation
import UIKit

protocol ArticleViewModelProtocol {
    
    /*
     Use this method to load data.
     **/
    func loadData()
    
    /*
     Use this method to refresh data.
     **/
    func refreshData()
    
    /*
     Use this method to get ArticleDetailViewModel.
     **/
    func getArticleDetailViewModel(fromIndex index: Int) -> ArticleDetailViewModel?
}
