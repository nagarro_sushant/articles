//
//  APIManagerProtocol.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import Foundation

protocol APIManagerProtocol {
    
    /*
     Use this method to get articles from server.
     **/
    func getArticlesFromServer(completionBlock: @escaping GetArticlesFromServerCompletion)
}
