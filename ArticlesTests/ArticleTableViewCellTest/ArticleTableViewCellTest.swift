//
//  ArticleTableViewCellTest.swift
//  ArticlesTests
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import XCTest
@testable import Articles

class ArticleTableViewCellTest: XCTestCase {
    
    var itemCell: ArticleTableViewCell!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        itemCell = ArticleTableViewCell()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        itemCell = nil
        super.tearDown()
    }
    
    func testPlotDataWithNilTitle() {
        var item = getDummyItem()
        item.title = nil
        itemCell.plotDataOnCell(withCellArticle: item)
        XCTAssertTrue(itemCell.articleTitleLabel.text == AppConstants.kEmptyString)
    }
    
    func testPlotDataWithNilAbstract() {
        var item = getDummyItem()
        item.abstract = nil
        itemCell.plotDataOnCell(withCellArticle: item)
        XCTAssertTrue(itemCell.articleDescriptionLabel.text == AppConstants.kEmptyString)
    }
    
    func testPlotDataWithNilTime() {
        var item = getDummyItem()
        item.published_date = nil
        itemCell.plotDataOnCell(withCellArticle: item)
        XCTAssertTrue(itemCell.articleDateLabel.text == AppConstants.kEmptyString)
    }
    
    func testPlotDataWithNotNilItem() {
        let item = getDummyItem()
        itemCell.plotDataOnCell(withCellArticle: item)
        XCTAssertTrue(itemCell.articleDescriptionLabel.text != AppConstants.kEmptyString)
    }

}

// MARK: - Extension ArticleTableViewCellTest
extension ArticleTableViewCellTest {
    
    // MARK: - Get Dummy Article For Test
    func getDummyItem() -> Article {
        return Article.init(id: 0011, url: "", title: "Article dummy title", abstract: "Article dummy abstract.", published_date: "2019-08-10")
    }
}
