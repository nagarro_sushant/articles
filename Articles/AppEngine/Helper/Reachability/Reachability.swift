//
//  Reachability.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import Foundation
import Alamofire

class Reachability: ReachabilityProtocol {
    
    // MARK: - Check internet connection
    func isInternetConnected() -> Bool {
        guard let manager = NetworkReachabilityManager() else {
            return false
        }
        return manager.isReachable
    }
}
