//
//  ArticleViewController.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import UIKit

struct ArticleListConstant {
    static let estimatedRowHeight: CGFloat = 100
    static let cellUniqueIdentifier = "ArticleCell"
}

class ArticleViewController: UIViewController {
    
    // MARK: - UI Element
    var articlesTableView = UITableView()
    
    // MARK: - Variables
    var refreshControl: UIRefreshControl = {
        let refControl = UIRefreshControl()
        return refControl
    }()
    
    var viewModel = ArticleViewModel()

    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        self.setupTableView()
        self.layoutTableView()
        self.setupCompletionHandlers()
        self.viewModel.loadData()
    }
    
    // MARK: - Setup Navigation Bar
    func setupNavigationBar() {
        self.title = ScreenTitles.articles
    }

    // MARK: - Add Constraints To TableView
    func layoutTableView() {
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(articlesTableView)
        
        let cornerAnchorForTableView = ConrnerAnchor(top: (self.topLayoutGuide.bottomAnchor, ViewConstraintConstants.zeroPaddingConstant),
                                                     bottom: (self.bottomLayoutGuide.topAnchor, ViewConstraintConstants.zeroPaddingConstant),
                                                     left: (self.view.leftAnchor, ViewConstraintConstants.zeroPaddingConstant),
                                                     right: (self.view.rightAnchor, ViewConstraintConstants.zeroPaddingConstant)
        )
        articlesTableView.addConstraints(cornerConstraints: cornerAnchorForTableView,
                                 centerY: nil,
                                 centerX: nil,
                                 height: ViewConstraintConstants.zeroPaddingConstant,
                                 width: ViewConstraintConstants.zeroPaddingConstant
        )
    }

    // MARK: - Setup Table View
    func setupTableView() {
        articlesTableView.delegate = self
        articlesTableView.dataSource = self
        articlesTableView.register(ArticleTableViewCell.self, forCellReuseIdentifier: ArticleListConstant.cellUniqueIdentifier)
        articlesTableView.separatorStyle = .none
        refreshControl.addTarget(self, action: #selector(self.refreshTableViewData), for: .valueChanged)
        articlesTableView.refreshControl = refreshControl
    }

    // MARK: - Refresh Table View Data
    @objc func refreshTableViewData() {
        viewModel.refreshData()
    }
    
    // MARK: - Navigate to Article Detail Screen
    func navigateToArticleDetail(withDetailVM detailVM: ArticleDetailViewModel) {
        let detailVC = ArticleDetailViewController.init(detailVM)
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    // MARK: - Setup Completion Handlers
    func setupCompletionHandlers() {
        self.handleCompletionWithSuccess()
        self.handleCompletionWithNoData()
        self.handleCompletionWithError()
        self.handleInternetError()
    }
    
    // MARK: - Handle Completion With Success
    func handleCompletionWithSuccess() {
        viewModel.handleCompletionWithSuccess = {[weak self] in
            DispatchQueue.main.async {
                self?.articlesTableView.reloadData()
                self?.stopLoader()
            }
        }
    }
    
    // MARK: - Handle Completion With No Data
    func handleCompletionWithNoData() {
        viewModel.handleCompletionWithNoData = {[weak self] in
            DispatchQueue.main.async {
                CommonClass.shared.showToastWithTitle(messageBody: AlertMessage.noData, onViewController: self)
                self?.stopLoader()
            }
        }
    }
    
    // MARK: - Handle Completion with Error
    func handleCompletionWithError() {
        viewModel.handleCompletionWithError = {[weak self] (error) in
            DispatchQueue.main.async {
                CommonClass.shared.showToastWithTitle(messageBody: error.localizedDescription, onViewController: self)
                self?.stopLoader()
            }
        }
    }
    
    // MARK: - Handle Internet error
    func handleInternetError() {
        viewModel.handleInternetError = {[weak self] in
            DispatchQueue.main.async {
                CommonClass.shared.showToastWithTitle(messageBody: AlertMessage.noInternet, onViewController: self)
                self?.stopLoader()
            }
        }
    }
    
    // MARK: - Stop Loader
    func stopLoader() {
        CommonClass.shared.hideLoader()
        refreshControl.endRefreshing()
    }
}

// MARK: - Extension For Table View Delegate And Datasource Methods
extension ArticleViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return ArticleListConstant.estimatedRowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticleListConstant.cellUniqueIdentifier, for: indexPath) as! ArticleTableViewCell
        cell.plotDataOnCell(withCellArticle: viewModel.articles[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewModel = self.viewModel.getArticleDetailViewModel(fromIndex: indexPath.row) {
            self.navigateToArticleDetail(withDetailVM: viewModel)
        }
    }
}
