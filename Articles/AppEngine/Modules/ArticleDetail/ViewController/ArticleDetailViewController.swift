//
//  ArticleDetailViewController.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import UIKit

class ArticleDetailViewController: UIViewController {

    // MARK: - UI Elements
    var articleImageView = UIImageView()
    var articleTitleLabel = UILabel()
    var articleDescriptionLabel = UILabel()
    var articleDateLabel = UILabel()

    // MARK: - Variables
    var articleDetailVM: ArticleDetailViewModel
    init(_ articleDetailVM: ArticleDetailViewModel) {
        self.articleDetailVM = articleDetailVM
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        self.setupUIElements()
        self.layoutUIElements()
        self.plotData()
    }
    
    // MARK: - Setup Navigation Bar
    func setupNavigationBar() {
        self.title = ScreenTitles.articleDetail
    }

    // MARK: - Setup UI Elements
    func setupUIElements() {
        self.view.backgroundColor = .white
        
        articleImageView.clipsToBounds = true
        articleImageView.contentMode = UIView.ContentMode.scaleAspectFill
        
        articleTitleLabel.numberOfLines = 0
        articleTitleLabel.textAlignment = .left
        articleTitleLabel.textColor = UIColor.black

        articleDescriptionLabel.numberOfLines = 0
        articleDescriptionLabel.textAlignment = .left
        articleDescriptionLabel.textColor = UIColor.darkGray

        articleDateLabel.numberOfLines = 1
        articleDateLabel.textAlignment = .right
        articleDateLabel.textColor = UIColor.lightGray
    }
    
    // MARK: - Add Constraints to UI Elements
    func layoutUIElements() {
        self.view.addSubview(articleImageView)
        self.view.addSubview(articleTitleLabel)
        self.view.addSubview(articleDescriptionLabel)
        self.view.addSubview(articleDateLabel)

        let cornerAnchorForImageView = ConrnerAnchor(top: (self.topLayoutGuide.bottomAnchor, ViewConstraintConstants.paddingConstant),
                                                   bottom: (nil, ViewConstraintConstants.zeroPaddingConstant),
                                                   left: (nil, ViewConstraintConstants.zeroPaddingConstant),
                                                   right: (nil, ViewConstraintConstants.zeroPaddingConstant)
        )
        articleImageView.addConstraints(cornerConstraints: cornerAnchorForImageView,
                               centerY: nil,
                               centerX: view.centerXAnchor,
                               height: ViewConstraintConstants.imageHeight,
                               width: ViewConstraintConstants.imageHeight
        )
        
        let cornerAnchorForTitleLabel = ConrnerAnchor(top: (self.articleImageView.bottomAnchor, ViewConstraintConstants.paddingConstant),
                                                      bottom: (nil, ViewConstraintConstants.zeroPaddingConstant),
                                                      left: (self.view.leftAnchor, ViewConstraintConstants.paddingConstant),
                                                      right: (self.view.rightAnchor, ViewConstraintConstants.paddingConstant)
        )
        articleTitleLabel.addConstraints(cornerConstraints: cornerAnchorForTitleLabel,
                                  centerY: nil,
                                  centerX: nil,
                                  height: ViewConstraintConstants.zeroPaddingConstant,
                                  width: ViewConstraintConstants.zeroPaddingConstant
        )
        
        let cornerConstraintForArticleDescriptionLabel = ConrnerAnchor(top: (self.articleTitleLabel.bottomAnchor, ViewConstraintConstants.paddingConstant),
                                                           bottom: (nil, ViewConstraintConstants.zeroPaddingConstant),
                                                           left: (self.view.leftAnchor, ViewConstraintConstants.paddingConstant),
                                                           right: (self.view.rightAnchor, ViewConstraintConstants.paddingConstant))
        articleDescriptionLabel.addConstraints(cornerConstraints: cornerConstraintForArticleDescriptionLabel,
                                     centerY: nil,
                                     centerX: nil,
                                     height: ViewConstraintConstants.zeroPaddingConstant,
                                     width: ViewConstraintConstants.zeroPaddingConstant
        )
        
        let cornerConstraintForArticleDateLabel = ConrnerAnchor(top: (nil, ViewConstraintConstants.zeroPaddingConstant),
                                                              bottom: (self.bottomLayoutGuide.topAnchor, ViewConstraintConstants.paddingConstant),
                                                              left: (self.view.leftAnchor, ViewConstraintConstants.paddingConstant),
                                                              right: (self.view.rightAnchor, ViewConstraintConstants.paddingConstant)
        )
        articleDateLabel.addConstraints(cornerConstraints: cornerConstraintForArticleDateLabel,
                                          centerY: nil,
                                          centerX: nil,
                                          height: ViewConstraintConstants.zeroPaddingConstant,
                                          width: ViewConstraintConstants.zeroPaddingConstant
        )
    }
    
    // MARK: - Plot Data
    func plotData() {
        self.articleImageView.image = AppPlaceholderImageConstants.article
        
        guard let title = articleDetailVM.item.title else {
            self.articleTitleLabel.text = AppConstants.kEmptyString
            return
        }
        self.articleTitleLabel.text = title
        
        guard let desc = articleDetailVM.item.abstract else {
            self.articleDescriptionLabel.text = AppConstants.kEmptyString
            return
        }
        self.articleDescriptionLabel.text = desc
        
        guard let date = articleDetailVM.item.published_date else {
            self.articleDateLabel.text = AppConstants.kEmptyString
            return
        }
        self.articleDateLabel.text = date
    }
}
