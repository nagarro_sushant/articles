//
//  ArticleViewModel.swift
//  Articles
//
//  Created by sushantkumar01 on 03/02/20.
//  Copyright © 2020 sushantkumar01. All rights reserved.
//

import Foundation

class ArticleViewModel {
    
    internal typealias Completion = () -> Void
    internal typealias CompletionWithError = (_ error: Error) -> Void

    var handleCompletionWithSuccess: Completion?
    var handleCompletionWithNoData: Completion?
    var handleCompletionWithError: CompletionWithError?
    var handleInternetError: Completion?

    // MARK: - Variables
    var articles = [Article]()
    var isRequestInProgress = false

    //Dependency Injection to make ApiManager & Reachability Testable using Mocking
    var apiManager: APIManagerProtocol
    var reachability: ReachabilityProtocol
    
    init(_ apiManager: APIManagerProtocol = APIManager(),
         reachability: ReachabilityProtocol = Reachability()) {
        self.apiManager = apiManager
        self.reachability = reachability
    }

    // MARK: - Fetch Articles from Server
    private func fetchArticlesFromServer(isRefreshData refresh: Bool) {
        guard reachability.isInternetConnected() else {
            handleInternetError?()
            return
        }
        if self.articles.isEmpty && !refresh {
            CommonClass.shared.showLoader()
        }
        isRequestInProgress = true
        apiManager.getArticlesFromServer {[weak self] (result: Result<AllArticle, Error>) in
            self?.isRequestInProgress = false
            switch result {
            case .success(let items):
                guard !items.results.isEmpty else {
                    self?.handleCompletionWithNoData?()
                    return
                }
                self?.onSuccessRefreshItems(withItems: items.results)
            case .failure(let error):
                self?.handleCompletionWithError?(error)
            }
        }
    }
    
    // MARK: - Handle Refresh Success
     func onSuccessRefreshItems(withItems items: [Article]) {
        articles = items
        self.handleCompletionWithSuccess?()
    }
}

// MARK: - Extension ArticleViewModel
extension ArticleViewModel: ArticleViewModelProtocol {
    
    // MARK: - Load Data
    func loadData() {
        guard !isRequestInProgress else {
            return
        }
        self.fetchArticlesFromServer(isRefreshData: false)
    }
    
    // MARK: - Refresh Data
    func refreshData() {
        guard !isRequestInProgress else {
            self.handleCompletionWithSuccess?()
            return
        }
        self.fetchArticlesFromServer(isRefreshData: true)
    }
    
    // MARK: - Get Article Detail View Model
    func getArticleDetailViewModel(fromIndex index: Int) -> ArticleDetailViewModel? {
        return (articles.count > index) ?
            ArticleDetailViewModel.init(withItem: articles[index]) :
        nil
    }
}
